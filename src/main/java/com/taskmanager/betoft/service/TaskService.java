package com.taskmanager.betoft.service;

import com.taskmanager.betoft.model.category.Category;
import com.taskmanager.betoft.model.category.CategoryRepository;
import com.taskmanager.betoft.model.group.Group;
import com.taskmanager.betoft.model.group.TaskList;
import com.taskmanager.betoft.model.task.SubTask;
import com.taskmanager.betoft.model.task.Task;
import com.taskmanager.betoft.model.task.TaskRepository;
import com.taskmanager.betoft.repository.CategoryRepositoryImpl;
import com.taskmanager.betoft.repository.TaskRepositoryImpl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaskService {
    private static TaskService instance;

    private Group currentGroup;
    private TaskList currentTaskList;
    private Task currentTask;
    private final TaskRepository taskRepository = new TaskRepositoryImpl();
    private final CategoryRepository categoryRepository = new CategoryRepositoryImpl();


    private TaskService() {

    }

    public static synchronized TaskService getInstance() {
        if (instance == null) {
            instance = new TaskService();
        }
        return instance;
    }

    public Task getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(Task currentTask) {
        this.currentTask = currentTask;
    }

    public Group getCurrentGroup() {
        return currentGroup;
    }

    public void setCurrentGroup(Group currentGroup) {
        this.currentGroup = currentGroup;
    }

    public TaskList getCurrentTaskList() {
        return currentTaskList;
    }

    public void setCurrentTaskList(TaskList currentTaskList) {
        this.currentTaskList = currentTaskList;
    }

    public List<Task> getTasksByUserIdAndListId(String userId, String listId) {
        return taskRepository.findTaskByUserIdAndListId(userId, listId);
    }

    public Map<String, String> getCategories() {
        return categoryRepository.findAll().stream()
                .collect(Collectors.toMap(Category::getId, Category::getName));
    }

    public String toggleTaskCompletion(String taskId) {
        return taskRepository.toggleTaskCompletion(taskId);
    }


    public SubTask saveSubtask(SubTask subTask) {
        SubTask newSubtask = taskRepository.saveSubTask(currentTask.getId(), subTask);
        updateSubTasks();
        return newSubtask;
    }

    public void updateSubTasks() {
        currentTask.setSubTasks(taskRepository.findSubtaskByTaskId(currentTask.getId()));
    }

    public Task saveTask(Task task) {
        return taskRepository.saveTask(task);
    }

    public void updateTaskFields(String name, String description, Date dueDate, String categoryId) {
        taskRepository.updateTaskFields(currentTask.getId(),
                name,
                description,
                dueDate,
                categoryId);
    }

    public void updateSubTaskFields(String subTaskId, boolean isCompleted, String name) {
        taskRepository.updateSubTaskFields(currentTask.getId(), subTaskId, isCompleted, name);
    }

    public void deleteSubTaskById(String subTaskId) {
        taskRepository.deleteSubTaskById(currentTask.getId(), subTaskId);
    }

    public void deleteTaskById(String taskId) {
        taskRepository.deleteTaskById(taskId);
    }

    public int countCompletedTask(String userId){
        return taskRepository.countCompletedTasks(userId);
    }

    public int countIncompleteTask(String userId){
        return taskRepository.countIncompletedTasks(userId);
    }
}
