package com.taskmanager.betoft.service;

import com.taskmanager.betoft.model.role.RoleRepository;
import com.taskmanager.betoft.model.user.User;
import com.taskmanager.betoft.model.user.UserRepository;
import com.taskmanager.betoft.repository.RoleRepositoryImpl;
import com.taskmanager.betoft.repository.UserRepositoryImpl;

import java.util.Map;

public class UserService {
    private static UserService instance;
    private final UserRepository userRepository = new UserRepositoryImpl();
    private final RoleRepository roleRepository = new RoleRepositoryImpl();
    private User currentUser;

    private UserService() {

    }

    public static synchronized UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public User authenticateUser(String userName, String password) {
        currentUser = userRepository.authenticateUser(userName, password);
        return currentUser;
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public boolean deleteUser(String userId) {
        String resultMessage = userRepository.deleteUserById(userId);
        return resultMessage.startsWith("Usuario eliminado");
    }
    public User updateUser(User user){
        currentUser = userRepository.updateUser(user);
        return currentUser;
    }

    public Map<String, String> getAllRoles() {
        return roleRepository.findAllRoles();
    }

    public String getCurrentUserRoleName() {
        if (currentUser != null) {
            String roleId = currentUser.getRole();
            return roleRepository.findRoleNameById(roleId);
        } else {
            return null;
        }
    }
}
