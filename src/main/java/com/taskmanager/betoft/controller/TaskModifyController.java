package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.HelloApplication;
import com.taskmanager.betoft.ViewFactory;
import com.taskmanager.betoft.model.task.SubTask;
import com.taskmanager.betoft.model.task.Task;
import com.taskmanager.betoft.service.TaskService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.ZoneId;
import java.util.*;


public class TaskModifyController implements Initializable {
    @FXML
    private Stage stage;
    @FXML
    private HomeController homeStage;
    @FXML
    private Button modifyButton;
    @FXML
    private Button cancelButton;
    @FXML
    private TextField taskName;
    @FXML
    private DatePicker taskDueDate;

    @FXML
    private ChoiceBox<String> taskCategoryChoiceBox;
    @FXML
    private TextArea taskDescription;
    @FXML
    private ScrollPane scrollTask;
    @FXML
    private GridPane subtaskGridPane;
    @FXML
    private Button addSubtask;

    private final TaskService taskService = TaskService.getInstance();
    private Task task = taskService.getCurrentTask();
    private List<SubTask> subTasks = new ArrayList<>();
    private Map<String, String> categories = taskService.getCategories();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        taskName.setText(task.getName());
        taskDueDate.setValue(task.getDueDate());
        taskDescription.setText(task.getDescription());

        taskCategoryChoiceBox.setStyle("-fx-font-size: 16px; -fx-background-color: white; -fx-background-radius: 30; ");
        taskCategoryChoiceBox.getItems().clear();
        taskCategoryChoiceBox.getItems().addAll(categories.values());
        taskCategoryChoiceBox.setValue(categories.get(task.getCategoryId()));


        uploadSubTask();

    }

    public void uploadSubTask() {
        subTasks.clear();
        subtaskGridPane.getChildren().clear();
        taskService.updateSubTasks();
        subTasks = task.getSubTasks();
        int column = 0;
        int row = 1;
        try {
            for (SubTask subTask : subTasks) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(ViewFactory.class.getResource("fxml/subtaskItem-view.fxml"));
                AnchorPane anchorPane = fxmlLoader.load();

                SubtaskItemController itemController = fxmlLoader.getController();
                itemController.setData(subTask, this);

                subtaskGridPane.add(anchorPane, column, row++);

                subtaskGridPane.setMinWidth(Region.USE_COMPUTED_SIZE);
                subtaskGridPane.setPrefWidth(Region.USE_COMPUTED_SIZE);
                subtaskGridPane.setMaxWidth(Region.USE_PREF_SIZE);

                subtaskGridPane.setMinHeight(Region.USE_COMPUTED_SIZE);
                subtaskGridPane.setPrefHeight(Region.USE_COMPUTED_SIZE);
                subtaskGridPane.setMaxHeight(Region.USE_PREF_SIZE);

                GridPane.setMargin(anchorPane, new Insets(10));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void addSubtaskButtonOnAction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("fxml/add-subtask-view.fxml"));
        Parent root = loader.load();
        AddSubtaskController controller = loader.getController();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        controller.setStage(stage);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();

        FXMLLoader modifyLoader = new FXMLLoader(HelloApplication.class.getResource("fxml/modify-task-view.fxml"));
        Parent modifyRoot = modifyLoader.load();
        TaskModifyController modifyController = modifyLoader.getController();
        Scene modifyScene = new Scene(modifyRoot);
        Stage modifyStage = new Stage();
        modifyController.setStage(modifyStage, homeStage);
        modifyStage.initModality(Modality.APPLICATION_MODAL);
        modifyStage.setScene(modifyScene);
        modifyStage.show();
        this.stage.close();
    }

    @FXML
    public void cancelModifyTaskButtonOnAction(ActionEvent event) {
        stage.close();
    }

    @FXML
    public void modifyTaskButtonOnAction(ActionEvent event){
        updateDataTask();
        homeStage.clearData();
        stage.close();
    }


    public  void updateDataTask() {

        String newTaskName = taskName.getText();
        Date newDueDate =   new Date(Date.from(taskDueDate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()).getTime());
        String newDescription = taskDescription.getText();
        String newCategory =  categories.entrySet().stream()
                .filter(entry -> entry.getValue().equals(taskCategoryChoiceBox.getValue()))
                .findFirst()
                .get()
                .getKey();

        taskService.updateTaskFields(newTaskName,newDescription,newDueDate,newCategory);

    }

    public void setStage(Stage stage, HomeController homeStage) {
        this.stage = stage;
        this.homeStage = homeStage;
    }
}







