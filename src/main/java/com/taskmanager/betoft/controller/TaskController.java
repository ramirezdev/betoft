package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.model.task.SubTask;
import com.taskmanager.betoft.model.task.Task;
import com.taskmanager.betoft.service.TaskService;
import com.taskmanager.betoft.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class TaskController {
    @FXML
    private Stage stage;

    @FXML
    private TextField taskName;

    @FXML
    private DatePicker taskDueDate;
    @FXML
    private Label taskCategoryLabel;
    @FXML
    private ChoiceBox <String> taskCategoryChoiceBox;
    @FXML
    private Button cancelButton;
    @FXML
    private Button addButton;
    @FXML
    private TextArea taskDescription;
    @FXML
    private Label validationLabel;
    private final UserService userService = UserService.getInstance();
    private final TaskService taskService = TaskService.getInstance();
    private final Map<String, String> categoryMap = taskService.getCategories();


    @FXML
    public void initialize() {
        taskCategoryChoiceBox.setStyle("-fx-font-size: 16px; -fx-background-color: white; -fx-background-radius: 30; ");
        taskCategoryChoiceBox.getItems().clear();
        taskCategoryChoiceBox.getItems().addAll(categoryMap.values().stream().toList());

        taskCategoryLabel.setVisible(true);
        taskCategoryChoiceBox.valueProperty().addListener((obs, oldVal, newVal) -> {
            taskCategoryLabel.setVisible(newVal == null);
        });

    }
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void cancelTaskButtonOnAction(ActionEvent event) {
        if (stage != null) {
            stage.close();
        }
    }


    public void addTask(){
        if (taskName.getText().isEmpty() || taskDueDate.getValue()== null || taskCategoryChoiceBox.getValue() == null || taskDescription.getText().isEmpty()) {
            validationLabel.setText("Please fill in all fields");
            return;
        }
        List<SubTask> subTasks = new ArrayList<>();
        taskService.saveTask(new Task(userService.getCurrentUser().getId(),
                new Date(Date.from(taskDueDate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()).getTime()),
                taskDescription.getText(),
                taskName.getText(),
                categoryMap.entrySet().stream()
                        .filter(entry -> entry.getValue().equals(taskCategoryChoiceBox.getValue()))
                        .findFirst()
                        .get()
                        .getKey(),
                taskService.getCurrentTaskList().getId(),
                subTasks, false));
        stage.close();
    }

}
