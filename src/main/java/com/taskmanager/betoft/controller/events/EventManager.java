package com.taskmanager.betoft.controller.events;

import java.util.HashMap;
import java.util.Map;

public class EventManager {
    private static EventManager eventManager;
    private Map<EventType, EventListener> eventListeners = new HashMap<>();

    private EventManager() {
    }

    public static EventManager getInstance() {
        if (eventManager == null) {
            eventManager = new EventManager();

        }
        return eventManager;
    }

    public void subscribeToEvents(EventListener eventListener, EventType eventName) {
        eventListeners.put(eventName, eventListener);
    }

    public void notifyEvent(EventType eventName) {
        EventListener eventListener = eventListeners.get(eventName);
        eventListener.onEvent();
    }
}
