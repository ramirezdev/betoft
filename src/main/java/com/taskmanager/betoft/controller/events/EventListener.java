package com.taskmanager.betoft.controller.events;

public interface EventListener {
    void onEvent();
}
