package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.ViewFactory;
import com.taskmanager.betoft.model.task.Task;
import com.taskmanager.betoft.service.TaskService;
import com.taskmanager.betoft.service.UserService;
import com.taskmanager.betoft.HelloApplication;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static com.taskmanager.betoft.ViewFactory.openNewScene;


public class HomeController implements Initializable {
    @FXML
    private Button taskManagerButton;
    @FXML
    private Button userMenuButton;
    @FXML
    private Button addTaskButton;
    @FXML
    private Label numberTaskLabel;
    @FXML
    private Text groupName;
    @FXML
    private Text listName;
    @FXML
    private ScrollPane scroll;
    @FXML
    private GridPane grid;
    private List<Task> tasks = new ArrayList<>();
    private final TaskService taskService = TaskService.getInstance();
    private final UserService userService = UserService.getInstance();

    public void taskManagerButtonOnAction(ActionEvent event) {
        openNewScene(taskManagerButton, "fxml/task-manager-view.fxml");

    }

    public void userMenuButtonOnAction(ActionEvent event) {
        openNewScene(userMenuButton, "fxml/user-view.fxml");

    }

    public void addTaskButtonOnAction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("fxml/new-task-view.fxml"));
        Parent root = loader.load();
        TaskController controller = loader.getController();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        controller.setStage(stage);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();
        openNewScene(addTaskButton, "fxml/home-view.fxml");
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        groupName.setText(taskService.getCurrentGroup().getName());
        listName.setText(taskService.getCurrentTaskList().getName());
        clearData();
    }

    public void clearData() {
        tasks.clear();
        grid.getChildren().clear();
        tasks.addAll(taskService.getTasksByUserIdAndListId(userService.getCurrentUser().getId(), taskService.getCurrentTaskList().getId()));
        numberTaskLabel.setText(String.valueOf(tasks.size()));
        int column = 0;
        int row = 1;
        try {
            for (Task task : tasks) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(ViewFactory.class.getResource("fxml/taskItem-view.fxml"));
                AnchorPane anchorPane = fxmlLoader.load();

                TaskItemController itemController = fxmlLoader.getController();
                itemController.setData(task, new Stage(), this);

                grid.add(anchorPane, column, row++);
                grid.setMinWidth(Region.USE_COMPUTED_SIZE);
                grid.setPrefWidth(Region.USE_COMPUTED_SIZE);
                grid.setMaxWidth(Region.USE_PREF_SIZE);

                grid.setMinHeight(Region.USE_COMPUTED_SIZE);
                grid.setPrefHeight(Region.USE_COMPUTED_SIZE);
                grid.setMaxHeight(Region.USE_PREF_SIZE);

                GridPane.setMargin(anchorPane, new Insets(10));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
