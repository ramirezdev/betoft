package com.taskmanager.betoft.controller.forms;

import com.taskmanager.betoft.model.group.Group;
import com.taskmanager.betoft.service.UserService;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.UUID;

public class GroupFormController extends Form {
    @FXML
    public Button cancelButton;
    @FXML
    public Button saveButton;


    @Override
    public void getInformation() {
        UserService userService = UserService.getInstance();
        String currentUserId = userService.getCurrentUser().getId();
        if (item == null){
            Group newGroup = new Group(UUID.randomUUID().toString(), itemText.getText(), new ArrayList<>(), currentUserId);
            item = newGroup;
        }else {
            Group group = (Group) item;
            group.setName(itemText.getText());
        }
    }

    @Override
    public void initialize() {
        if (item != null){
            itemText.setText(item.toString());
        }
        Image image = new Image(getClass().getResourceAsStream("/com/taskmanager/betoft/images/pencil.png"));
        labelHead.setText("Add Group");
        anchorChoose.setDisable(true);
        iconHead.setImage(image);
    }
}
