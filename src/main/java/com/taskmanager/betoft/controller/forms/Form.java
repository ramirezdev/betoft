package com.taskmanager.betoft.controller.forms;

import com.taskmanager.betoft.controller.events.EventManager;
import com.taskmanager.betoft.controller.events.EventType;
import com.taskmanager.betoft.model.group.Group;
import com.taskmanager.betoft.model.group.GroupRepository;
import com.taskmanager.betoft.model.group.TaskList;
import com.taskmanager.betoft.repository.GroupRepositoryImpl;
import com.taskmanager.betoft.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.List;

public abstract class Form {
    @FXML
    protected Button cancelButton;
    @FXML
    protected TextField itemText = new TextField();
    @FXML
    protected AnchorPane anchorChoose;
    @FXML
    protected ComboBox<Group> deployGroups = new ComboBox<>();
    @FXML
    protected ImageView iconHead;
    @FXML
    protected Label labelHead;
    @FXML
    protected Label alertLabel;
    protected Object item;


    public abstract void getInformation();

    public void setItem(Object itemParam){
        item = itemParam;
        if (itemParam instanceof TaskList){
            deployGroups.setValue(findGroup());
        }
        itemText.setText(itemParam.toString());
    }
    public void initialize() {
    }

    public void cancel(ActionEvent actionEvent) {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    public void submit(ActionEvent event) {
        if (itemText.getText().isBlank()) {
            alertLabel.setText("The name cannot be empty");
        } else if (labelHead.getText().equals("Add List") && deployGroups.getValue() == null) {
            alertLabel.setText("Please select a group");
        } else {
            getInformation();
            saveItem();
            EventManager eventManager = EventManager.getInstance();
            eventManager.notifyEvent(EventType.LOAD_TREEVIEW);
            cancel(event);
        }
    }

    protected void saveItem() {
        GroupRepositoryImpl groupRepository = new GroupRepositoryImpl();
        if (item instanceof Group) {
            checkGroup(groupRepository);
        } else {
            checkTaskList(groupRepository);
        }
    }

    protected void checkGroup(GroupRepositoryImpl groupRepository) {
        String userId = UserService.getInstance().getCurrentUser().getId();
        List<Group> groups = groupRepository.findGroupsByUserId(userId);

        if (groups.contains((Group) item)) {
            groupRepository.updateGroup((Group) item);
        } else {
            groupRepository.save((Group) item);
        }
    }

    protected Group findGroup() {
        Group group = null;
        GroupRepositoryImpl groupRepository = new GroupRepositoryImpl();
        List<Group> groups = groupRepository.findGroupsByUserId(UserService.getInstance().getCurrentUser().getId());
        for (Group groupOption : groups) {
            if (groupOption.containsTaskList((TaskList) item)) {
                group = groupOption;
            }
        }
        return group;
    }

    protected void checkTaskList(GroupRepository groupRepository) {
        Group group = deployGroups.getValue();
        Group groupFounded = findGroup();
        if (group != null && group.equals(groupFounded) ) {
            groupRepository.updateTaskList(group.getId(), (TaskList) item);
        } else if (groupFounded != null) {
            groupRepository.moveTaskListToGroup(groupFounded.getId(), group.getId(), (TaskList) item);
        } else if(group != null){
            groupRepository.saveTaskList(group.getId(), (TaskList) item);
        }
    }
}
