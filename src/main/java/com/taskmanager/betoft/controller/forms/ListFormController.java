package com.taskmanager.betoft.controller.forms;

import com.taskmanager.betoft.model.group.Group;
import com.taskmanager.betoft.model.group.TaskList;
import com.taskmanager.betoft.repository.GroupRepositoryImpl;
import com.taskmanager.betoft.service.UserService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.bson.types.ObjectId;

import java.util.List;


public class ListFormController extends Form {

    @Override
    public void getInformation() {
        if (item == null){
            ObjectId objectId = new ObjectId();
            String idString = objectId.toHexString();
            item = new TaskList(idString, itemText.getText());
        }else {
            TaskList taskList = (TaskList) item;
            taskList.setName(itemText.getText());
        }
    }

    public void initialize() {
        if (item instanceof TaskList){
            deployGroups.setValue(findGroup());
            itemText.setText(item.toString());
        }
        GroupRepositoryImpl groupRepository = new GroupRepositoryImpl();
        UserService userService = UserService.getInstance();
        String userId = userService.getCurrentUser().getId();
        List<Group> groupOfList = groupRepository.findGroupsByUserId(userId);
        ObservableList<Group> items = FXCollections.observableArrayList(groupOfList);
        deployGroups.getItems().addAll(items);
    }
}
