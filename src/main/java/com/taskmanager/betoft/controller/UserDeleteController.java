package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import static com.taskmanager.betoft.ViewFactory.openNewScene;

public class UserDeleteController {
    @FXML
    private Button cancelDeleteButton;
    @FXML
    private Button UserDeleteButton;
    @FXML
    private Label deleteUserMessage;

    private final UserService userService = UserService.getInstance();


    public void cancelDeleteButtonOnAction(ActionEvent event) {
        openNewScene(cancelDeleteButton, "fxml/user-view.fxml");

    }

    public void UserDeleteButtonOnAction(ActionEvent event) {
        deleteUser();
    }

    public void deleteUser() {
        String userId = userService.getCurrentUser().getId();
        try {
            userService.deleteUser(userId);
            deleteUserMessage.setText("User Deleted!!");
            openNewScene(UserDeleteButton, "fxml/add-user-view.fxml");


        } catch (RuntimeException e) {
            String fullMessage = e.getMessage();

            int lastIndex = fullMessage.lastIndexOf(":");
            String lastPart = fullMessage.substring(lastIndex + 1).trim();
            deleteUserMessage.setText(lastPart + "!!");
        }

    }


}
