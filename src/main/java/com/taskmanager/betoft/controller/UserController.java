package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.service.TaskService;
import com.taskmanager.betoft.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import static com.taskmanager.betoft.ViewFactory.openNewScene;

public class UserController {

    @FXML
    private Button backToHomeButton;
    @FXML
    private Button userUpdateDataButton;
    @FXML
    private Button userDeleteDataButton;
    @FXML
    private Label counterCompletedTasks;
    @FXML
    private Label counterPendingTasks;
    @FXML
    private Label nameUser;
    @FXML
    private Label roleUser;
    private final TaskService taskService = TaskService.getInstance();
    private final UserService userService = UserService.getInstance();

    @FXML
    public void initialize() {
        nameUser.setText(userService.getCurrentUser().getUserName());
        roleUser.setText(userService.getCurrentUserRoleName());



        setCounterCompletedTasks();
        setCounterPendingTasks();
    }

    public void backToHomeButtonOnAction(ActionEvent event) {
        openNewScene(backToHomeButton, "fxml/home-view.fxml");

    }

    public void userUpdateDataButtonOnAction(ActionEvent event) {
        openNewScene(userUpdateDataButton, "fxml/user-update-view.fxml");

    }

    public void userDeleteDataButtonOnAction(ActionEvent event) {
        openNewScene(userDeleteDataButton, "fxml/user-delete-view.fxml");

    }

    public void setCounterCompletedTasks() {
        String  completedTasks = String.valueOf(taskService.countCompletedTask(userService.getCurrentUser().getId()));
        System.out.println("completedTask: "+ completedTasks);
        counterCompletedTasks.setText(completedTasks);

    }

    public void setCounterPendingTasks() {
        String  pendingTasks = String.valueOf(taskService.countIncompleteTask(userService.getCurrentUser().getId()));
        System.out.println("PendingTask: "+ pendingTasks );
        counterPendingTasks.setText(pendingTasks);
    }

}
