package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.model.user.User;
import com.taskmanager.betoft.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import static com.taskmanager.betoft.ViewFactory.openNewScene;

public class    UserUpdateController {


    @FXML
    private Button backToUserView;
    @FXML
    private Button userUpdateButton;
    @FXML
    private TextField newUserName;
    @FXML
    private PasswordField newPassword;
    @FXML
    private Label updateUserMessage;
    private final UserService userService = UserService.getInstance();

    public void backToUserViewButtonOnAction(ActionEvent event) {
        openNewScene(backToUserView, "fxml/user-view.fxml");

    }
    public void userUpdateButtonOnAction(ActionEvent event) {
        updateUser();
    }

    public void updateUser() {
        String userNameText = newUserName.getText();
        String userPasswordText = newPassword.getText();
        User currentUser = userService.getCurrentUser();

        try {
            currentUser.setUserName(userNameText);
            currentUser.setPassword(userPasswordText);
            userService.updateUser(currentUser);
            updateUserMessage.setText("Updated User!!");
            openNewScene(userUpdateButton, "fxml/login-view.fxml");

        }catch (RuntimeException e) {
            String fullMessage = e.getMessage();
            int lastIndex = fullMessage.lastIndexOf(":");
            String lastPart = fullMessage.substring(lastIndex + 1).trim();
            updateUserMessage.setText(lastPart + "!!");
        }
    }

}
