package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.model.role.RoleRepository;
import com.taskmanager.betoft.model.user.User;
import com.taskmanager.betoft.model.user.UserRepository;
import com.taskmanager.betoft.repository.RoleRepositoryImpl;
import com.taskmanager.betoft.repository.UserRepositoryImpl;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class HelloController implements Initializable {
    @FXML
    private Label welcomeText;

    private UserRepository userRepository = new UserRepositoryImpl();
    private RoleRepository repository = new RoleRepositoryImpl();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<User> users = userRepository.findAllUsers();
        System.out.println(users.toString());
        welcomeText.setText(users.get(0).getUserName());
        User userAuth = userRepository.authenticateUser("ads", "asd");
        System.out.println(userAuth.getAuthenticated());
        System.out.println(userAuth.getId());
        System.out.println(userAuth.getUserName());
    }

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }
}