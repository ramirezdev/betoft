package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.model.user.User;
import com.taskmanager.betoft.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.HashMap;
import java.util.Map;

import static com.taskmanager.betoft.ViewFactory.openNewScene;

public class AddUserController {
    @FXML
    public Label userRolLabel;
    @FXML
    public Label addUserMessage;
    @FXML
    private Button addUserButton;
    @FXML
    private Button backToLoginButton;
    @FXML
    private TextField userNameAddUser;
    @FXML
    private PasswordField passwordAddUser;
    @FXML
    private ChoiceBox<String> userRolChoiceBox;
    private Map<String, String> userRoleMap = new HashMap<>();
    private final UserService userService = UserService.getInstance();


    @FXML
    public void initialize() {
        userRoleMap = userService.getAllRoles();
        userRolChoiceBox.getItems().addAll(userRoleMap.keySet());

        userRolLabel.setVisible(true);


        userRolChoiceBox.valueProperty().addListener((obs, oldVal, newVal) -> {

            userRolLabel.setVisible(newVal == null);
        });


        userNameAddUser.textProperty().addListener((observable, oldValue, newValue) -> {
            userNameAddUser.setStyle("-fx-background-color: transparent; -fx-border-color: white; -fx-border-width: 0px 0px 2px 0px; -fx-text-fill: black;");

        });
        passwordAddUser.textProperty().addListener((observable, oldValue, newValue) -> {
            passwordAddUser.setStyle("-fx-background-color: transparent; -fx-border-color: white; -fx-border-width: 0px 0px 2px 0px; -fx-text-fill: black;");

        });
    }

    public void addUserButtonOnAction(ActionEvent event) {
        if (!userNameAddUser.getText().isBlank() && !passwordAddUser.getText().isBlank()) {
            addUser();
        } else {
            addUserMessage.setText("Please enter user and password");
        }
    }
    public void backToLoginButtonOnAction(ActionEvent event) {
        openNewScene(backToLoginButton,"fxml/login-view.fxml");

    }

    public void addUser() {
        String userText = userNameAddUser.getText();
        String passwordText = passwordAddUser.getText();
        String userRolText = userRoleMap.get(userRolChoiceBox.getValue());
        User newuser = new User(userText, passwordText, userRolText);
        try {
            userService.saveUser(newuser);
            addUserMessage.setText("User created!!");
            openNewScene(addUserButton, "fxml/login-view.fxml");


        } catch (RuntimeException e) {
            String fullMessage = e.getMessage();

            int lastIndex = fullMessage.lastIndexOf(":");
            String lastPart = fullMessage.substring(lastIndex + 1).trim();
            addUserMessage.setText(lastPart + "!!");
        }
    }
}
