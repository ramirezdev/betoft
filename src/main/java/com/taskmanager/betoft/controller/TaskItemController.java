package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.ViewFactory;
import com.taskmanager.betoft.model.task.Task;
import com.taskmanager.betoft.service.TaskService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Map;

public class TaskItemController {
    @FXML
    private Stage stage;
    @FXML
    private HomeController homeStage;
    @FXML
    private Label categoryTask;
    @FXML
    private Label description;
    @FXML
    private CheckBox checkCompletion;
    @FXML
    private Button editButton;
    @FXML
    private Button deleteButton;

    private final TaskService taskService = TaskService.getInstance();
    private final Map<String, String> categories = taskService.getCategories();
    private Task task;

    public void setData(Task task, Stage stage, HomeController homeStage) {
        this.stage = stage;
        this.task = task;
        this.homeStage = homeStage;
        categoryTask.setText(categories.get(task.getCategoryId()));
        description.setText(task.getName());
        checkCompletion.setSelected(task.getCompleted());
    }

    public void checkCompletionCheckBoxOnAction(MouseEvent event) {
        taskService.toggleTaskCompletion(task.getId());
    }

    public void editButtonOnAction(ActionEvent event) throws IOException {
        taskService.setCurrentTask(task);

        FXMLLoader loader = new FXMLLoader(ViewFactory.class.getResource("fxml/modify-task-view.fxml"));
        Parent root = loader.load();
        TaskModifyController controller = loader.getController();
        Scene scene = new Scene(root);
        Stage taskStage = new Stage();
        controller.setStage(taskStage, this.homeStage);
        taskStage.initModality(Modality.APPLICATION_MODAL);
        taskStage.setScene(scene);
        taskStage.showAndWait();
        homeStage.clearData();
        taskStage.close();
    }

    public void deleteButtonOnAction(ActionEvent event) {
        taskService.deleteTaskById(task.getId());
        homeStage.clearData();
        stage.close();
    }

}
