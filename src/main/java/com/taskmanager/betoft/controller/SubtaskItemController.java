package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.model.task.SubTask;
import com.taskmanager.betoft.service.TaskService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

public class SubtaskItemController {

    @FXML
    private Label nameSubtask;
    @FXML
    private CheckBox checkCompletion;
    @FXML
    private Button deleteSubtaskButton;
    @FXML
    private TaskModifyController taskModifyController;
    private final TaskService taskService = TaskService.getInstance();
    private SubTask subtask;


    public void setData(SubTask subtask, TaskModifyController taskModifyController) {
        nameSubtask.setText(subtask.getDescription());
        checkCompletion.setSelected(subtask.getCompleted());
        this.subtask = subtask;
        this.taskModifyController = taskModifyController;
    }

    public void updateSubTaskCompletion(ActionEvent event) {
        taskService.updateSubTaskFields(subtask.getId(), !subtask.getCompleted() , nameSubtask.getText());
    }

    public void deleteSubtask(ActionEvent event) {
        taskService.deleteSubTaskById(subtask.getId());
        taskModifyController.uploadSubTask();
    }
}
