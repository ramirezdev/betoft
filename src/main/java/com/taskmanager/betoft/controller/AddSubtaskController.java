package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.model.task.SubTask;
import com.taskmanager.betoft.service.TaskService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class AddSubtaskController {
    @FXML
    private TextField nameSubtask;
    @FXML
    private Button addSubtaskButton;
    @FXML
    private Button cancelSubtaskButton;
    @FXML
    private Label validationLabel;
    private Stage stage;
    private final TaskService taskService = TaskService.getInstance();

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void addSubtaskButtonOnAction(ActionEvent event) {
        addSubtask();
    }

    public void addSubtask() {
        String nameSubtaskText = nameSubtask.getText();
        if (nameSubtaskText.isEmpty()) {
            validationLabel.setText("Please fill in all fields");
            return;
        }
        SubTask newSubtask = new SubTask(nameSubtaskText, false);
        taskService.saveSubtask(newSubtask);
        stage.close();

    }

    @FXML
    public void cancelSubtaskButtonOnAction(ActionEvent event) {
        if (stage != null) {
            stage.close();
        }
    }

}
