package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.model.user.User;
import com.taskmanager.betoft.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import static com.taskmanager.betoft.ViewFactory.openNewScene;

public class LoginController {
    @FXML
    private Button addUserButton;
    @FXML
    private TextField userNameLogin;
    @FXML
    private PasswordField passwordLogin;

    @FXML
    private Label loginMessage;

    @FXML
    private Button loginButton;
    private final UserService userService = UserService.getInstance();


    public void loginButtonOnAction(ActionEvent event) {
        if (!userNameLogin.getText().isBlank() && !passwordLogin.getText().isBlank()) {
            validateLogin();
        } else {
            loginMessage.setText("Please enter user and password");
        }
    }

    public void validateLogin() {

        String userText = userNameLogin.getText();
        String passwordText = passwordLogin.getText();
        User auntenticatedUser = userService.authenticateUser(userText, passwordText);

        if (auntenticatedUser.getAuthenticated()) {
            System.out.println("Welcome To Betoft");
            openNewScene(loginButton, "fxml/task-manager-view.fxml");

        } else {
            loginMessage.setText("Invalid Credatials!!");
        }
    }

    public void addUserButtonOnAction(ActionEvent event){
        openNewScene(addUserButton, "fxml/add-user-view.fxml");
    }

    @FXML
    public void initialize() {
        userNameLogin.textProperty().addListener((observable, oldValue, newValue) -> {
            userNameLogin.setStyle("-fx-background-color: transparent; -fx-border-color: white; -fx-border-width: 0px 0px 3px 0px; -fx-text-fill: white;");
        });
        passwordLogin.textProperty().addListener((observable, oldValue, newValue) -> {
            passwordLogin.setStyle("-fx-background-color: transparent; -fx-border-color: white; -fx-border-width: 0px 0px 3px 0px; -fx-text-fill: white;");
        });
    }

}

