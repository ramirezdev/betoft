package com.taskmanager.betoft.controller;

import com.taskmanager.betoft.HelloApplication;
import com.taskmanager.betoft.controller.events.EventListener;
import com.taskmanager.betoft.controller.events.EventManager;
import com.taskmanager.betoft.controller.events.EventType;
import com.taskmanager.betoft.controller.forms.Form;
import com.taskmanager.betoft.controller.forms.GroupFormController;
import com.taskmanager.betoft.controller.forms.ListFormController;
import com.taskmanager.betoft.model.group.Group;
import com.taskmanager.betoft.model.group.TaskList;
import com.taskmanager.betoft.repository.GroupRepositoryImpl;
import com.taskmanager.betoft.service.TaskService;
import com.taskmanager.betoft.service.UserService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static com.taskmanager.betoft.ViewFactory.openNewScene;

public class TaskManagerController implements EventListener {

    @FXML
    public Button burgerBack;
    @FXML
    public MenuItem groupOption;
    @FXML
    public MenuItem listOption;
    @FXML
    private TreeView<Object> treeGroup;


    private void showWindow(Form formController, Object item) throws IOException {
        if (item != null) {
            formController.setItem(item);
        }
        FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("fxml/form-view.fxml"));
        loader.setController(formController);
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();
    }

    private void loadTreeGroups() {
        TreeItem<Object> root = treeGroup.getRoot();
        root.getChildren().clear();
        GroupRepositoryImpl groupRepository = new GroupRepositoryImpl();
        UserService userService = UserService.getInstance();
        List<Group> groupList = groupRepository.findGroupsByUserId(userService.getCurrentUser().getId());
        for (Group group : groupList) {
            List<TaskList> taskLists = group.getTaskLists();
            TreeItem<Object> itemGroup = new TreeItem<>(group);
            loadBranch(itemGroup, taskLists);
            root.getChildren().add(itemGroup);
        }
    }

    private void loadBranch(TreeItem<Object> rootItem, List<?> childrenList) {
        ObservableList<TreeItem<Object>> branches = FXCollections.observableArrayList();
        childrenList.forEach(branch -> branches.add(new TreeItem<>(branch)));
        rootItem.getChildren().setAll(branches);
    }

    public void backHome(ActionEvent actionEvent) {
        if (!Objects.isNull(TaskService.getInstance().getCurrentTaskList())) {
            openNewScene(burgerBack, "fxml/home-view.fxml");
        }
    }

    public void modifyGroup(Object item) throws IOException {
        Form groupFormController = new GroupFormController();
        showWindow(groupFormController, item);
    }

    public void modifyList(Object item) throws IOException {
        Form listFormController = new ListFormController();
        showWindow(listFormController, item);
    }

    @FXML
    void addGroup(ActionEvent event) throws IOException {
        Form groupFormController = new GroupFormController();
        showWindow(groupFormController, null);
    }

    @FXML
    void addList(ActionEvent event) throws IOException {
        Form listFormController = new ListFormController();
        showWindow(listFormController, null);
    }

    public void initialize() {
        EventManager eventManager = EventManager.getInstance();
        eventManager.subscribeToEvents(this, EventType.LOAD_TREEVIEW);
        TreeItem<Object> root = new TreeItem<>("groups");
        treeGroup.setRoot(root);
        treeGroup.setShowRoot(false);
        loadTreeGroups();

        treeGroup.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                TreeItem<Object> parentItem = newValue.getParent();
                if (parentItem.getValue() instanceof Group groupSelected) {
                    TaskList taskListSelected = (TaskList) newValue.getValue();
                    TaskService taskService = TaskService.getInstance();
                    taskService.setCurrentGroup(groupSelected);
                    taskService.setCurrentTaskList(taskListSelected);
                } else {
                    System.out.println("Selected item has no parent Group type.");
                }
            }
        });
    }

    @FXML
    void delete(ActionEvent event) {
        TreeItem<Object> item = treeGroup.getSelectionModel().getSelectedItem();
        Object itemValue = item.getValue();
        GroupRepositoryImpl groupRepository = new GroupRepositoryImpl();
        if (itemValue instanceof Group) {
            Group group = (Group) itemValue;
            groupRepository.deleteGroupById(group.getId());
        }else {
            Group group = (Group) item.getParent().getValue();
            TaskList taskList = (TaskList) itemValue;
            groupRepository.deleteTaskList(group.getId(), taskList.getId());
        }
        loadTreeGroups();
    }

    @FXML
    void edit(ActionEvent event) throws IOException {
        Object item = treeGroup.getSelectionModel().getSelectedItem().getValue();
        if (item instanceof Group) {
            modifyGroup(item);
        } else {
            modifyList(item);
        }
    }

    @Override
    public void onEvent() {
        loadTreeGroups();

    }
}


