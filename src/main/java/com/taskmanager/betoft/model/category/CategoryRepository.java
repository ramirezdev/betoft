package com.taskmanager.betoft.model.category;

import java.util.List;

public interface CategoryRepository {
    List<Category> findAll();
}
