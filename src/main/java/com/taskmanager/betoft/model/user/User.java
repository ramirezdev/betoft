package com.taskmanager.betoft.model.user;

public class User {

    private String id;
    private String userName;
    private String password;
    private String role;
    private Boolean isAuthenticated;

    public User(String id, String userName, String role, Boolean isAuthenticated) {
        this.id = id;
        this.userName = userName;
        this.role = role;
        this.isAuthenticated = isAuthenticated;
    }

    public User(String userName, String password, String role) {
        this.userName = userName;
        this.password = password;
        this.role = role;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getAuthenticated() {
        return isAuthenticated;
    }

    public void setAuthenticated(Boolean authenticated) {
        isAuthenticated = authenticated;
    }
}