package com.taskmanager.betoft.model.user;

import java.util.List;

public interface UserRepository {
    User authenticateUser(String userName, String password);
    List<User> findAllUsers();
    User save(User user);
    User updateUser(User updatedUser);
    String deleteUserById(String userId);
}
