package com.taskmanager.betoft.model.task;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Task {
    private String id;
    private String name;
    private String description;
    private Date dueDate;
    private Boolean isCompleted;
    private String userId;
    private String categoryId;
    private String listId;
    private List<SubTask> subTasks;

    public Task(String id, String name, String description, Date dueDate, Boolean isCompleted, String userId, String categoryId, String listId, List<SubTask> subTasks) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dueDate = dueDate;
        this.isCompleted = isCompleted;
        this.userId = userId;
        this.categoryId = categoryId;
        this.listId = listId;
        this.subTasks = subTasks;
    }

    public Task(String userId, Date dueDate, String description, String name, String categoryId, String listId, List<SubTask> subTasks, Boolean isCompleted) {
        this.description = description;
        this.name = name;
        this.dueDate = dueDate;
        this.isCompleted = isCompleted;
        this.userId = userId;
        this.categoryId = categoryId;
        this.listId = listId;
        this.subTasks = subTasks;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDueDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        return LocalDate.parse(dueDate.toString(), formatter);
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Boolean getCompleted() {
        return isCompleted;
    }

    public void setCompleted(Boolean completed) {
        isCompleted = completed;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public List<SubTask> getSubTasks() {
        return subTasks;
    }

    public void setSubTasks(List<SubTask> subTasks) {
        this.subTasks = subTasks;
    }

    @Override
    public String toString() {
        return id;
    }
}
