package com.taskmanager.betoft.model.task;

public class SubTask {
    private String id;
    private String description;
    private Boolean isCompleted;

    public SubTask(String id, String description, Boolean isCompleted) {
        this.id = id;
        this.description = description;
        this.isCompleted = isCompleted;
    }
    public SubTask(String name, Boolean isCompleted) {
        this.description = name;
        this.isCompleted = isCompleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCompleted() {
        return isCompleted;
    }

    public void setCompleted(Boolean completed) {
        isCompleted = completed;
    }
}
