package com.taskmanager.betoft.model.task;

import java.util.Date;
import java.util.List;

public interface TaskRepository {
    Task saveTask(Task task);
    Task findTaskById(String id);
    List<Task> findTaskByUserIdAndListId(String userId, String listId);
    String deleteTaskById(String id);
    String toggleTaskCompletion(String id);
    SubTask saveSubTask(String taskId, SubTask subTask);
    List<SubTask> findSubtaskByTaskId(String subtaskId);
    String deleteSubTask(String taskId, String subTaskId);
    void updateTaskFields(String taskId, String name, String description, Date dueDate, String categoryId);
    void updateSubTaskFields(String taskId, String subTaskId, boolean isCompleted, String name);
    void deleteSubTaskById(String taskId, String subTaskId);

    int countCompletedTasks(String userId);
    int countIncompletedTasks(String userId);
}
