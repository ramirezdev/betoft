package com.taskmanager.betoft.model.role;

import java.util.Map;

public interface RoleRepository {
    Map<String, String> findAllRoles();
    String findRoleNameById(String roleId);
}
