package com.taskmanager.betoft.model.group;

import java.util.List;
import java.util.Objects;

public class Group {
    private String id;
    private String name;
    private List<TaskList> taskLists;
    private String userId;

    public Group(String id, String name, List<TaskList> taskLists, String userId) {
        this.id = id;
        this.name = name;
        this.taskLists = taskLists;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TaskList> getTaskLists() {
        return taskLists;
    }

    public void setTaskLists(List<TaskList> taskLists) {
        this.taskLists = taskLists;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean containsTaskList(TaskList taskList) {
        return taskLists.contains(taskList);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(id, group.id);
    }
}
