package com.taskmanager.betoft.model.group;

import java.util.Objects;

public class TaskList {
    private String id;
    private String name;

    public TaskList(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public TaskList(String name){
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskList taskList = (TaskList) o;
        return Objects.equals(id, taskList.id);
    }
}
