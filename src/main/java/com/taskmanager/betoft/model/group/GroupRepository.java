package com.taskmanager.betoft.model.group;

import java.util.List;

public interface GroupRepository {
    Group save(Group group);
    Group updateGroup(Group updatedGroup);
    String deleteGroupById(String groupId);
    List<Group> findGroupsByUserId(String userId);
    void saveTaskList(String groupId, TaskList taskList);
    TaskList updateTaskList(String groupId, TaskList updatedTaskList);
    TaskList moveTaskListToGroup(String sourceGroupId, String destinationGroupId, TaskList taskList);

    void deleteTaskList(String groupId, String taskListId);
}
