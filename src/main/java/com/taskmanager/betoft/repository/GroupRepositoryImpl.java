package com.taskmanager.betoft.repository;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.taskmanager.betoft.model.group.Group;
import com.taskmanager.betoft.model.group.GroupRepository;
import com.taskmanager.betoft.model.group.TaskList;
import com.taskmanager.betoft.repository.configuration.MongoClientConnection;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class GroupRepositoryImpl implements GroupRepository {

    private final MongoClient mongoClient = MongoClientConnection.getInstance().getClient();
    private final MongoDatabase database = mongoClient.getDatabase("Betoft");
    private final MongoCollection<Document> collection = database.getCollection("Group");


    @Override
    public Group save(Group group) {
        Document document = new Document("name", group.getName())
                .append("userId", group.getUserId())
                .append("lists", new ArrayList<>());

        collection.insertOne(document);

        return group;
    }

    @Override
    public Group updateGroup(Group updatedGroup) {
        Document query = new Document("_id", new ObjectId(updatedGroup.getId()));
        Document updateDocument = new Document("$set",
                new Document("name", updatedGroup.getName()));

        collection.updateOne(query, updateDocument);

        return updatedGroup;
    }

    @Override
    public String deleteGroupById(String groupId) {
        Document query = new Document("_id", new ObjectId(groupId));
        collection.deleteOne(query);
        return "Group deleted successfully";
    }

    @Override
    public List<Group> findGroupsByUserId(String userId) {
        List<Group> groups = new ArrayList<>();
        Document query = new Document("userId", userId);
        for (Document document : collection.find(query)) {
            groups.add(documentToGroup(document));
        }
        return groups;
    }

    @Override
    public void saveTaskList(String groupId, TaskList taskList) {
        Document query = new Document("_id", new ObjectId(groupId));
        Document groupDocument = collection.find(query).first();
        if (groupDocument != null) {
            List<Document> lists = (List<Document>) groupDocument.get("lists");
            ObjectId taskListId = new ObjectId(taskList.getId());
            Document taskListDocument = new Document("_id", taskListId).append("name", taskList.getName());
            lists.add(taskListDocument);
            collection.updateOne(query, new Document("$set", new Document("lists", lists)));
        }
    }

    @Override
    public TaskList updateTaskList(String groupId, TaskList updatedTaskList) {
        Document query = new Document("_id", new ObjectId(groupId))
                .append("lists._id", new ObjectId(updatedTaskList.getId()));

        Document updateDocument = new Document("$set",
                new Document("lists.$.name", updatedTaskList.getName()));

        collection.updateOne(query, updateDocument);

        return updatedTaskList;
    }

    @Override
    public TaskList moveTaskListToGroup(String sourceGroupId, String destinationGroupId, TaskList taskList) {
        Document deleteQuery = new Document("_id", new ObjectId(sourceGroupId));
        Document pullUpdate = new Document("$pull", new Document("lists", new Document("_id", new ObjectId(taskList.getId()))));
        collection.updateOne(deleteQuery, pullUpdate);

        Document insertQuery = new Document("_id", new ObjectId(destinationGroupId));
        Document pushUpdate = new Document("$push", new Document("lists", new Document("_id", new ObjectId(taskList.getId())).append("name", taskList.getName())));
        collection.updateOne(insertQuery, pushUpdate);

        return taskList;
    }

    private Group documentToGroup(Document document) {
        if (document == null) {
            return null;
        }

        String id = document.getObjectId("_id").toString();
        String name = document.getString("name");
        String userId = document.getString("userId");
        List<Document> taskListsDocuments = (List<Document>) document.get("lists");
        List<TaskList> taskLists = new ArrayList<>();
        if (taskListsDocuments != null) {
            for (Document taskListDocument : taskListsDocuments) {
                String taskListId = taskListDocument.getObjectId("_id").toString();
                String taskName = taskListDocument.getString("name");
                taskLists.add(new TaskList(taskListId, taskName));
            }
        }

        return new Group(id, name, taskLists, userId);
    }

    @Override
    public void deleteTaskList(String groupId, String taskListId) {
        Document query = new Document("_id", new ObjectId(groupId));
        Document updateQuery = new Document("$pull", new Document("lists", new Document("_id", new ObjectId(taskListId))));
        collection.updateOne(query, updateQuery);
    }
}
