package com.taskmanager.betoft.repository;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.taskmanager.betoft.model.category.Category;
import com.taskmanager.betoft.model.category.CategoryRepository;
import com.taskmanager.betoft.repository.configuration.MongoClientConnection;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class CategoryRepositoryImpl implements CategoryRepository {
    private final MongoClient mongoClient = MongoClientConnection.getInstance().getClient();
    private final MongoDatabase database = mongoClient.getDatabase("Betoft");
    private final MongoCollection<Document> collection = database.getCollection("Category");


    @Override
    public List<Category> findAll() {
        List<Category> categories = new ArrayList<>();
        try (MongoCursor<Document> cursor = collection.find().iterator()) {
            while (cursor.hasNext()) {
                Document doc = cursor.next();
                String id = doc.getObjectId("_id").toString();
                String name = doc.getString("name");
                Category category = new Category(id, name);
                categories.add(category);
            }
        }
        return categories;
    }
}
