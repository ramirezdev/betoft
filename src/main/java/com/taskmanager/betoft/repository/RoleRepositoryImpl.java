package com.taskmanager.betoft.repository;

import com.taskmanager.betoft.model.role.RoleRepository;
import com.taskmanager.betoft.repository.configuration.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class RoleRepositoryImpl implements RoleRepository {

    private final MySQLConnection mySQLConnection = MySQLConnection.getInstance();

    @Override
    public Map<String, String> findAllRoles() {
        Map<String, String> rolesMap = new HashMap<>();
        String query = "SELECT id, name FROM role";
        try (Connection connection = mySQLConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                rolesMap.put(name, id);
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar roles de la base de datos: " + e.getMessage());
            throw new RuntimeException(e);
        }
        return rolesMap;
    }
    @Override
    public String findRoleNameById(String roleId){
        String query = "SELECT name FROM role WHERE id = ?";
        try (Connection connection = mySQLConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, roleId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("name");
            } else {
                return "Viewer";
            }
        } catch (SQLException e) {
            System.out.println("Error al recuperar el nombre del rol de la base de datos: " + e.getMessage());
            throw new RuntimeException(e);
        }

    }
}
