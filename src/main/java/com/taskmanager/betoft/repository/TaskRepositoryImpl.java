package com.taskmanager.betoft.repository;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.taskmanager.betoft.model.task.Task;
import com.taskmanager.betoft.model.task.TaskRepository;
import com.taskmanager.betoft.model.task.SubTask;
import com.taskmanager.betoft.repository.configuration.MongoClientConnection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TaskRepositoryImpl implements TaskRepository {

    private final MongoClient mongoClient = MongoClientConnection.getInstance().getClient();
    private final MongoDatabase database = mongoClient.getDatabase("Betoft");
    private final MongoCollection<Document> collection = database.getCollection("Task");


    @Override
    public Task saveTask(Task task) {
        Document document = new Document("description", task.getDescription())
                .append("name", task.getName())
                .append("dueDate", task.getDueDate())
                .append("isCompleted", task.getCompleted())
                .append("userId", task.getUserId())
                .append("categoryId", task.getCategoryId())
                .append("listId", task.getListId())
                .append("subTasks", task.getSubTasks());

        collection.insertOne(document);

        return task;
    }

    @Override
    public Task findTaskById(String id) {
        Document query = new Document("_id", new ObjectId(id));
        Document result = collection.find(query).first();
        return documentToTask(result);
    }

    @Override
    public List<Task> findTaskByUserIdAndListId(String userId, String listId) {
        List<Task> tasks = new ArrayList<>();
        Document query = new Document("userId", userId)
                .append("listId", listId);
        for (Document document : collection.find(query)) {
            tasks.add(documentToTask(document));
        }
        return tasks;
    }

    @Override
    public String deleteTaskById(String id) {
        Document query = new Document("_id", new ObjectId(id));
        collection.deleteOne(query);
        return "Task deleted successfully";
    }

    @Override
    public String toggleTaskCompletion(String id) {
        Document query = new Document("_id", new ObjectId(id));
        Document taskDocument = collection.find(query).first();
        if (taskDocument != null) {
            boolean isCompleted = taskDocument.getBoolean("isCompleted", false);
            boolean updatedCompletion = !isCompleted;
            Document updateDocument = new Document("$set", new Document("isCompleted", updatedCompletion));
            collection.updateOne(query, updateDocument);
            return "Task completion toggled successfully";
        } else {
            return "Task not found";
        }
    }

    @Override
    public SubTask saveSubTask(String taskId, SubTask subTask) {
        Document query = new Document("_id", new ObjectId(taskId));
        Document taskDocument = collection.find(query).first();
        if (taskDocument != null) {
            Document subTaskDocument = new Document("id", new ObjectId())
                    .append("description", subTask.getDescription())
                    .append("isCompleted", subTask.getCompleted());
            collection.updateOne(query, Updates.addToSet("subTask", subTaskDocument));
            return subTask;
        } else {
            return null;
        }
    }
    @Override
    public List<SubTask> findSubtaskByTaskId(String taskId) {
        List<SubTask> subTasks = new ArrayList<>();
        Document query = new Document("_id", new ObjectId(taskId));
        Document taskDocument = collection.find(query).first();
        if (taskDocument != null) {
            List<Document> subTasksDocument = (List<Document>) taskDocument.get("subTask");
            if (subTasksDocument != null) {
                for (Document subTaskDocument : subTasksDocument) {
                    String id = subTaskDocument.getObjectId("id").toString();
                    String description = subTaskDocument.getString("description");
                    Boolean isCompleted = subTaskDocument.getBoolean("isCompleted");
                    SubTask subTask = new SubTask(id, description, isCompleted);
                    subTasks.add(subTask);
                }
            }
        }
        return subTasks;
    }

    @Override
    public String deleteSubTask(String taskId, String subTaskId) {
        Document query = new Document("_id", new ObjectId(taskId));
        Document taskDocument = collection.find(query).first();
        if (taskDocument != null) {
            collection.updateOne(query, Updates.pull("subTasks", new Document("_id", subTaskId)));
            return "Subtask deleted successfully";
        } else {
            return "Task not found";
        }
    }

    @Override
    public void updateTaskFields(String taskId, String name, String description, Date dueDate, String categoryId) {
        Document query = new Document("_id", new ObjectId(taskId));
        Document updateDocument = new Document();
        if (name != null) {
            updateDocument.append("name", name);
        }
        if (description != null) {
            updateDocument.append("description", description);
        }
        if (dueDate != null) {
            updateDocument.append("dueDate", dueDate);
        }
        if (categoryId != null) {
            updateDocument.append("categoryId", categoryId);
        }
        Document updateQuery = new Document("$set", updateDocument);
        collection.updateOne(query, updateQuery);
    }

    @Override
    public void updateSubTaskFields(String taskId, String subTaskId, boolean isCompleted, String name) {
        Document query = new Document("_id", new ObjectId(taskId))
                .append("subTask.id", new ObjectId(subTaskId));
        Document updateDocument = new Document();
        if (isCompleted) {
            updateDocument.append("subTask.$.isCompleted", true);
        }
        if (name != null) {
            updateDocument.append("subTask.$.name", name);
        }
        Document updateQuery = new Document("$set", updateDocument);
        collection.updateOne(query, updateQuery);
    }

    @Override
    public void deleteSubTaskById(String taskId, String subTaskId) {
        Document query = new Document("_id", new ObjectId(taskId));
        Document updateQuery = new Document("$pull", new Document("subTask", new Document("id", new ObjectId(subTaskId))));
        collection.updateOne(query, updateQuery);
    }

    @Override
    public int countCompletedTasks(String userId) {
        MongoCollection<Document> collection = database.getCollection("Task");

        List<Bson> pipelineCompleted = Arrays.asList(
                Aggregates.match(Filters.and(
                        Filters.eq("isCompleted",true ),
                        Filters.eq("userId", userId)
                )),
                Aggregates.count("totalCompletedTasks")
        );
        AggregateIterable<Document> resultCompleted = collection.aggregate(pipelineCompleted);
        int completedCount = 0;
        for (Document doc : resultCompleted) {
            completedCount = doc.getInteger("totalCompletedTasks", 0);
        }

        return completedCount;
    }

    public int countIncompletedTasks(String userId) {
        MongoCollection<Document> collection = database.getCollection("Task");

        List<Bson> pipelineIncomplete = Arrays.asList(
                Aggregates.match(Filters.and(
                        Filters.eq("isCompleted", false),
                        Filters.eq("userId", userId)
                )),
                Aggregates.count("totalIncompleteTasks")
        );
        AggregateIterable<Document> resultIncomplete = collection.aggregate(pipelineIncomplete);
        int incompleteCount = 0;
        for (Document doc : resultIncomplete) {
            incompleteCount = doc.getInteger("totalIncompleteTasks", 0);
        }

        return incompleteCount;
    }




    private Task documentToTask(Document document) {
        if (document == null) {
            return null;
        }

        String id = document.getObjectId("_id").toString();
        String name = document.getString("name");
        String description = document.getString("description");
        Date dueDate = document.getDate("dueDate");
        Boolean isCompleted = document.getBoolean("isCompleted");
        String userId = document.getString("userId");
        String categoryId = document.getString("categoryId");
        String listId = document.getString("listId");
        List<SubTask> subTasks = documentToSubTasks(document);

        return new Task(id, name, description, dueDate, isCompleted, userId, categoryId, listId, subTasks);
    }

    private List<SubTask> documentToSubTasks(Document document) {
        List<SubTask> subTasks = new ArrayList<>();
        List<Document> subTasksDocument = (List<Document>) document.get("subTask");

        if (subTasksDocument != null) {
            for (Document subTaskDocument : subTasksDocument) {
                String id = subTaskDocument.getObjectId("id").toString();
                String description = subTaskDocument.getString("description");
                Boolean isCompleted = subTaskDocument.getBoolean("isCompleted");
                SubTask subTask = new SubTask(id, description, isCompleted);
                subTasks.add(subTask);
            }
        }

        return subTasks;
    }
}
