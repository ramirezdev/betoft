package com.taskmanager.betoft.repository;

import com.taskmanager.betoft.model.user.User;
import com.taskmanager.betoft.model.user.UserRepository;
import com.taskmanager.betoft.repository.configuration.MySQLConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepositoryImpl implements UserRepository {

    private final MySQLConnection mySQLConnection = MySQLConnection.getInstance();


    @Override
    public User authenticateUser(String userName, String password) {
        try {
            CallableStatement callableStatement = getCallableStatement(userName, password);

            String userId = callableStatement.getString(3);
            String roleId = callableStatement.getString(4);
            Boolean isAuthenticated = callableStatement.getBoolean(6);

                return new User(userId, userName, roleId, isAuthenticated);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error al autenticar el usuario", e);
        }
    }

    private CallableStatement getCallableStatement(String userName, String password) throws SQLException {
        Connection connection = mySQLConnection.getConnection();
        CallableStatement callableStatement = connection.prepareCall("{call AuthenticateUser(?, ?, ?, ?, ?, ?)}");
        callableStatement.setString(1, userName);
        callableStatement.setString(2, password);
        callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
        callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);
        callableStatement.registerOutParameter(5, java.sql.Types.VARCHAR);
        callableStatement.registerOutParameter(6, java.sql.Types.BOOLEAN);

        callableStatement.execute();
        return callableStatement;
    }

    @Override
    public List<User> findAllUsers() {
        List<User> userList = new ArrayList<>();
        try {
            Connection connection = mySQLConnection.getConnection();
            String query = "SELECT id, username, role FROM user_details";
            ResultSet resultSet = connection.createStatement().executeQuery(query);

            while (resultSet.next()) {
                String userId = resultSet.getString("id");
                String userName = resultSet.getString("username");
                String role = resultSet.getString("role");
                userList.add(new User(userId, userName, role, false));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error al obtener la lista de usuarios", e);
        }
        return userList;
    }

    @Override
    public User save(User user) {
        String query = "INSERT INTO user (id, username, password, roleId) VALUES (?, ?, ?, ?)";
        try (Connection connection = mySQLConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, UUID.randomUUID().toString());
            preparedStatement.setString(2, user.getUserName());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getRole());
            preparedStatement.executeUpdate();
            return user;
        } catch (SQLException e) {
            System.out.println("Error al guardar el usuario: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public User updateUser(User updatedUser) {
        String query = "UPDATE user SET username = ?, password = ?, roleId = ? WHERE id = ?";
        try (Connection connection = mySQLConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, updatedUser.getUserName());
            preparedStatement.setString(2, updatedUser.getPassword());
            preparedStatement.setString(3, updatedUser.getRole());
            preparedStatement.setString(4, updatedUser.getId());
            int rowsUpdated = preparedStatement.executeUpdate();
            if (rowsUpdated > 0) {
                return updatedUser;
            } else {
                System.out.println("No se encontró ningún usuario con el ID proporcionado: " + updatedUser.getId());
                return null;
            }
        } catch (SQLException e) {
            System.out.println("Error al actualizar el usuario: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public String deleteUserById(String userId) {
        String query = "DELETE FROM user WHERE id = ?";
        try (Connection connection = mySQLConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, userId);
            int rowsDeleted = preparedStatement.executeUpdate();
            if (rowsDeleted == 0) {
                return "No se encontró ningún usuario con el ID proporcionado: " + userId;
            } else {
                return "Usuario eliminado exitosamente: " + userId;
            }
        } catch (SQLException e) {
            System.out.println("Error al eliminar el usuario: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
