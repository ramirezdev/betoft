package com.taskmanager.betoft.repository.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnection {
    private static MySQLConnection instance;
    private Connection databaseLink;

    private MySQLConnection() {
    }

    public static synchronized MySQLConnection getInstance() {
        if (instance == null) {
            instance = new MySQLConnection();
        }
        return instance;
    }

    public Connection getConnection() {
        String host = "";
        String port = "";
        String user = "root";
        String passwd = "";
        String databaseName = "betoft";

        String connectionString = "jdbc:mysql://" + host + ":" + port + "/" + databaseName;

        System.out.println("Connecting Database....");

        try {
            databaseLink = DriverManager.getConnection(connectionString, user, passwd);
            System.out.println("Database Connected Succes");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
        return databaseLink;
    }
}
