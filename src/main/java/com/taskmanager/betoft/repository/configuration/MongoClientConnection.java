package com.taskmanager.betoft.repository.configuration;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

public class MongoClientConnection {
    private static MongoClientConnection instance;
    private MongoClient mongoClient;

    private MongoClientConnection() {
    }

    public static synchronized MongoClientConnection getInstance() {
        if (instance == null) {
            instance = new MongoClientConnection();
        }
        return instance;
    }

    public MongoClient getClient() {
        if (mongoClient == null) {
            try {
                String uri = "mongodb+srv://<userser>:<psw>@cluster0.xedpzun.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
                mongoClient = MongoClients.create(uri);
                System.out.printf("conexion establecida a mongo");
            }catch (Exception e){
                System.out.printf("conexion rehusada");
            }

        }
        return mongoClient;
    }
}
