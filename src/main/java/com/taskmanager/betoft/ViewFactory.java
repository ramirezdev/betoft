package com.taskmanager.betoft;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class ViewFactory {

    public static void openNewScene(Button sourceButton, String fxmlFileName) {
        Stage stage = (Stage) sourceButton.getScene().getWindow();
        stage.close();

        try {
            FXMLLoader loader = new FXMLLoader(ViewFactory.class.getResource(fxmlFileName));
            Parent root = loader.load();
            Scene scene = new Scene(root);

            Stage newStage = new Stage();
            newStage.setScene(scene);
            newStage.show();
        } catch (Exception e){
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static void closeApplication() {
        Platform.exit();
    }

}
