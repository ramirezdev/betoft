module com.taskmanager.betoft {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.j;
    requires org.mongodb.bson;
    requires org.mongodb.driver.core;
    requires org.mongodb.driver.sync.client;

    opens com.taskmanager.betoft to javafx.fxml;
    opens com.taskmanager.betoft.repository to javafx.fxml;
    opens com.taskmanager.betoft.repository.configuration to javafx.fxml;
    exports com.taskmanager.betoft;
    exports com.taskmanager.betoft.repository;
    exports com.taskmanager.betoft.repository.configuration;
    exports com.taskmanager.betoft.model.category;
    exports com.taskmanager.betoft.model.group;
    exports com.taskmanager.betoft.model.role;
    exports com.taskmanager.betoft.model.task;
    exports com.taskmanager.betoft.model.user;
    exports com.taskmanager.betoft.controller;
    opens com.taskmanager.betoft.controller to javafx.fxml;
    exports com.taskmanager.betoft.controller.forms;
    opens com.taskmanager.betoft.controller.forms to javafx.fxml;
}